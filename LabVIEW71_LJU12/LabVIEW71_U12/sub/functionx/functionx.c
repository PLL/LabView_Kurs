//---------------------------------------------------------------------------
//
//  functionx.c
//  
//  DLL which provides user defined trigger functions for LJlogger.         
//
//  support@labjack.com
//  7/2001
//----------------------------------------------------------------------
//

//#include <math.h>
#include "bitops.h"		//useful bit manipulation functions

//Exported Function:
//		functionx


//Declaration of the exported function.  Do not Modify.
long __cdecl functionx(float scaledData[8],		//In
					   long *stateIO,			//In/Out, 0-15
					   long *stateD,			//In/Out, 0-65535
					   unsigned long counter,	//In
					   double secondsI,			//In (integer part)
					   double secondsF,			//In (fractional part)
					   float *ao0,				//Out, 0-5
					   float *ao1,				//Out, 0-5
					   long *directionIO,		//Out, 0 or 1
					   long *directionD);		//Out, 0-65535


//Declarations for local functions here


//Define global variables here


//The following function (DllMain) is automatically called when
//a process or thread attaches or detaches.  In this case,
//it is called when LJlogger starts up since that will
//be a process attaching.
BOOL WINAPI DllMain(HINSTANCE hinstDLL,  // handle to DLL module
					DWORD fdwReason,     // reason for calling function
					LPVOID lpReserved)  // reserved 
{
	// Perform actions based on the reason for calling. 
	switch( fdwReason ) 
	{
		case DLL_PROCESS_ATTACH:
			//This case executes when LJlogger is first loaded.
			//This is a good place to initialize global variables.
			//If FALSE is returned the DLL will not load.

			break;

		case DLL_THREAD_ATTACH:         
			//Should never occur.
			break;        

		case DLL_THREAD_DETACH:
			//Should never occur.
			break;

		case DLL_PROCESS_DETACH:         
			//This case executes when LJlogger exits.
			break;    
	}
       return TRUE;
}


//Below is the user-definable function that can
//be modified.  The declaration cannot be modified.

//======================================================================
// functionx: User-definable function for LJlogger triggers.
//
//	Returns:	result		-0 for false, 1 for true (I32)
//	Inputs:		scaledData	-8 element array of scaled data (SGL).
//				*stateIO	-States of IO0-IO3 (I32).
//				*stateD		-States of D0-D15 (I32).
//				counter		-Value of the 32-bit counter (U32).
//				secondsi	-Integer portion of seconds since 1/1/1904 UT (DBL).
//				secondsf	-Fractional portion of seconds (DBL).
//	Outputs:	*stateD		-States for D0-D15 (I32).
//				*stateIO	-States for IO0-IO3 (I32).
//				*ao0		-Voltage for analog output 0 (SGL).
//				*ao1		-Voltage for analog output 1 (SGL).
//				*directionIO	-0=Input, 1=Output, for IO0-IO3 (I32).
//				*directionD		-0=Input, 1=Output, for D0-D15 (I32).
//----------------------------------------------------------------------
long __cdecl functionx(float scaledData[8],		//In
					   long *stateIO,			//In/Out, 0-15
					   long *stateD,			//In/Out, 0-65535
					   unsigned long counter,	//In
					   double secondsI,			//In (integer part)
					   double secondsF,			//In (fractional part)
					   float *ao0,				//Out, 0-5
					   float *ao1,				//Out, 0-5
					   long *directionIO,		//Out, 0 or 1
					   long *directionD)		//Out, 0-65535
{
	return 0;
}

