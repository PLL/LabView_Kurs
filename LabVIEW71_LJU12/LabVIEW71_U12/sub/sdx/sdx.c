//---------------------------------------------------------------------------
//
//  sdx.c
//  
//  DLL which provides user defined data scaling functions for LJlogger.         
//
//  support@labjack.com
//  7/2001
//----------------------------------------------------------------------
//

//#include <math.h>
#include "bitops.h"		//useful bit manipulation functions

//Exported Function:
//		sdx


//Declaration of the exported function.  Do not Modify.
float __cdecl sdx(float voltages[8],		//In
				  float multipliers[8],		//In
				  float offsets[8],			//In
				  long index);				//In (0-7)


//Declarations for local functions here


//Define global variables here
float volts=9999.0F;


//The following function (DllMain) is automatically called when
//a process or thread attaches or detaches.  In this case,
//it is called when LJlogger starts up since that will
//be a process attaching.
BOOL WINAPI DllMain(HINSTANCE hinstDLL,  // handle to DLL module
					DWORD fdwReason,     // reason for calling function
					LPVOID lpReserved)  // reserved 
{
	// Perform actions based on the reason for calling. 
	switch( fdwReason ) 
	{
		case DLL_PROCESS_ATTACH:
			//This case executes when LJlogger is first loaded.
			//This is a good place to initialize global variables.
			//If FALSE is returned the DLL will not load.

			break;

		case DLL_THREAD_ATTACH:         
			//Should never occur.
			break;        

		case DLL_THREAD_DETACH:
			//Should never occur.
			break;

		case DLL_PROCESS_DETACH:         
			//This case executes when LJlogger exits.
			break;    
	}
       return TRUE;
}


//Below is the user-definable function that can
//be modified.  The declaration cannot be modified.

//======================================================================
// sdx: User-definable function for LJlogger data scaling.
//
//	Returns:	scaledData
//	Input:		voltages
//				multipliers
//				offsets
//				index
//----------------------------------------------------------------------
float __cdecl sdx(float voltages[8],		//In
				  float multipliers[8],		//In
				  float offsets[8],			//In
				  long index)				//In (0-7)
{
	//This SDX provides smoothing.
	//No smoothing =>  c=1.0
	//1 second smoothing =>  c=0.10
	//5 second smoothing =>  c=0.02
	float c=0.02F;
	float reading;
	reading=(voltages[index]*multipliers[index])+offsets[index];
	if(volts!=9999.0)
	{
		volts=(volts*(1.0F-c))+(reading*c);
	}
	else
	{
		volts=reading;
	}
	return volts;
}
